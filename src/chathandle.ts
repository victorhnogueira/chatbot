import axios from "axios";

function start(client, socket) {
  // Catch ctrl+C
  process.on("SIGINT", function () {
    client.close();
  });

  try {
    client.onMessage(async (message: any) => {
      if (message.isGroupMsg === false) {
        const [phoneNumber, _] = message.from.split("@");
        const { name } = message.sender;

        // Start typing...resp.data
        client.startTyping(message.from);

        await client
          .sendText(message.from, `Boa noite ${name}, tudo bem?`)
          .then((result: any) => {})
          .catch((erro: any) => {
            // console.error("Error when sending: ", erro); //return object error
          })
          .finally(() => {
            // Stop typing
            // client.stopTyping(message.from);
          });

        await client
          .sendText(
            message.from,
            `Bem vindo ao nosso delivery 😍,
  Clique aqui para ver cardapio:
  https://plataformadelivery.com`
          )
          .then((result: any) => {})
          .catch((erro: any) => {
            // console.error("Error when sending: ", erro); //return object error
          })
          .finally(() => {
            // Stop typing
            // client.stopTyping(message.from);
          });

        /*
        await axios
          .post(
            "http://localhost:3333/chatbot/buyer",
            {
              businessId: client.session,
              phone: phoneNumber,
              name: name,
            },
            {
              headers: {
                "Content-Type": "application/json",
              },
            }
          )
          .then(async (resp) => {
            const { buyer } = resp.data;
  
            // Start typing...resp.data
            client.startTyping(message.from);
            await client
              .sendText(message.from, `Boa noite ${buyer.fullName}, tudo bem?`)
              .then((result: any) => {})
              .catch((erro: any) => {
                // console.error("Error when sending: ", erro); //return object error
              })
              .finally(() => {
                // Stop typing
                // client.stopTyping(message.from);
              });
  
            await client
              .sendText(
                message.from,
                `Bem vindo ao nosso delivery 😍,
  Clique aqui para ver cardapio:
  https://plataformadelivery.com`
              )
              .then((result: any) => {})
              .catch((erro: any) => {
                // console.error("Error when sending: ", erro); //return object error
              })
              .finally(() => {
                // Stop typing
                // client.stopTyping(message.from);
              });
          })
          .catch((error) => {
            console.log("error");
            console.log(error.response);
          });
          */
      }
    });
  } catch (error) {
    console.log(error);
    client.close();
  }
}

export default start;
