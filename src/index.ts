import dotenv from 'dotenv'
import { httpServer, io } from './server';
import chathandle from './chathandle'

enum ExitStatus {
  Failure = 1,
  Success = 0
}

dotenv.config()

process.on('unhandledRejection', (reason, promise) => {
  console.log(
    `App exiting due to an unhandled promise: ${promise} and reason: ${reason}`
  )

  throw reason
})

process.on('uncaughtException', error => {
  console.log(`App exiting due to an ancaught exception: ${error}`)
  process.exit(ExitStatus.Failure)
})

//
;(async () => {
  try {
    io.on('connection', socket => {
      const { business } = socket.handshake.query
    
      require("venom-bot").create(
        business,
        (base64Qr) => {
          var matches = base64Qr.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/)
    
          if (matches?.length !== 3) {
            socket.emit('qrcode', null)
            return new Error("Invalid input string");
          }
    
          socket.emit('qrcode', matches[2])
        },
        (statusSession: string) => {
          socket.emit('status', statusSession )
        },
        { logQR: false }
      )
      .then((client) => {
        chathandle(client, socket);
      })
      .catch((erro) => {
        console.log('deu ruim');
        console.log(erro);
      });
    })
    
    io.on("disconnect", (reason) => {
      console.log(`Saiu ${reason}`);
    });
    
    httpServer.listen(process.env.PORT)

    const ExitSignals: NodeJS.Signals[] = ['SIGINT', 'SIGTERM', 'SIGQUIT']

    for (const signal of ExitSignals) {
      process.on(signal, async () => {
        try {
          io.close()
          console.log(`App exited successfully`)
          process.exit(ExitStatus.Success)
        } catch (error) {
          console.log(`App exited with error: ${error}`)
          process.exit(ExitStatus.Failure)
        }
      })
    }
  } catch (error) {
    console.log(`App exited with error: ${error}`)
    process.exit(ExitStatus.Failure)
  }
})()