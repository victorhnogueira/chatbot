import express from 'express'
import * as socketio from 'socket.io'
import * as http from 'http'

const app = express()
export const httpServer = http.createServer(app)

export const io = new socketio.Server(httpServer, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"]
  }
})